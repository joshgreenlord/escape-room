// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EscapeRoomGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPESOMEROOMS_API AEscapeRoomGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
