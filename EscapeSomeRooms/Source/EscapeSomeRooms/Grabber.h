// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PhysicsEngine\PhysicsHandleComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPESOMEROOMS_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	//How far the player can reach
	float  Reach = 100.f;

	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	UInputComponent* InputComponent = nullptr;
	
	// Ray-cast and grab what is in reach
	void Grab();

	// Release the currently grabbed object
	void Release();

	//Find physics handle component 
	void FindPhysicsComponent();

	//Find the input component 
	void SetupInputComponent();

	// Return hit for first physics object in reach
	const FHitResult GetFirstPhysicsBodyInReach();

	/// Gets start and end of line-trace
	const FVector GetLineTraceEnd();

	const FVector GetLineTraceStart();
};
